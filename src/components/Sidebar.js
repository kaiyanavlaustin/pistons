import React from "react";
import "../css/Sidebar.css";
import { signOut } from "firebase/auth";
import { auth } from "./Firebase-config";
import { useNavigate } from "react-router-dom";



function Sidebar() {
  const navigate = useNavigate();

  const logout = async () => {
    await signOut(auth);
    navigate("/")
  };

  return (
    <div className='sidebar'>
      <div id='logo'>
        <div className='logo-smoke' alt='homeLogoPart1' />
        <div className='logo-tires' alt='homeLogoPart2' />
        <div className='logo-body' alt='homeLogoPart3' />
        <button onClick={logout}> Sign Out </button>
      </div>

      {/* <SidebarOption active Icon={HomeIcon} text="Home" />
      <SidebarOption Icon={SearchIcon} text="Explore" />
      <SidebarOption Icon={NotificationsNoneIcon} text="Notifications" />
      <SidebarOption Icon={MailOutlineIcon} text="Messages" />
      <SidebarOption Icon={BookmarkBorderIcon} text="Bookmarks" />
      <SidebarOption Icon={ListAltIcon} text="Lists" />
      <SidebarOption Icon={PermIdentityIcon} text="Profile" />
      <SidebarOption Icon={MoreHorizIcon} text="More" /> */}

      {/* Button -> Tweet */}
      {/* <Button variant="outlined" className="sidebar__tweet" fullWidth>
        Tweet
      </Button> */}
    </div>
  );
}

export default Sidebar;
