import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyDa5J69HANxcomx2_KSWpSmbwKyyJpBO3c",
  authDomain: "tkm-pistons.firebaseapp.com",
  projectId: "tkm-pistons",
  storageBucket: "tkm-pistons.appspot.com",
  messagingSenderId: "289812235901",
  appId: "1:289812235901:web:5b9625f095d33e033cd314",
  measurementId: "G-21HB6G8SL8",
};

const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
