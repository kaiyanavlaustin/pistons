import React from "react";
import { auth } from "../components/Firebase-config";
import db from "../components/Firebase";
import { useNavigate } from "react-router-dom";
import { useState } from "react";

export default function Profile() {
  const user = auth.currentUser;
  const navigate = useNavigate();

  // make variables that will have the value of the form input
  const [displayName, setDisplayName] = useState("");
  const [userName, setUserName] = useState("");
  const [avatar, setAvatar] = useState("");

  const updateProfile = (e) => {
    e.preventDefault();

    db.collection("users").add({
      UUID: user.uid,
      displayName: displayName,
      userName: userName,
      avatar: avatar,
      emailAddress: user.email,
    });
    navigate("/home");
    //navigate to home
  };

  return (
    <div>
      <h1>Complete your Profile </h1>
      <form>
        <p>{user.uid}</p>
        <br />
        <br />
        <p>Display Name</p>
        <input
          placeholder='displayName here'
          onChange={(e) => setDisplayName(e.target.value)}
        />
        <p>{displayName}</p>
        {/*change value of DisplayName as user types into input*/}

        <br />
        <br />
        <p>User Name</p>
        <input
          placeholder='userName here'
          onChange={(e) => setUserName(e.target.value)}
        />
        {/*change value of UserName as user types into input*/}

        <br />
        <br />
        <p>Avatar</p>
        <input
          placeholder='avatar here place image URL here'
          onChange={(e) => setAvatar(e.target.value)}
        />
        {/*change value of Avatar as user types into input*/}

        <br />

        <button onClick={updateProfile}>submit</button>
      </form>
    </div>
  );
}
