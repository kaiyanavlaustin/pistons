import { auth } from "./Firebase-config";
import React, { forwardRef, useState } from "react";
import "../css/Post.css";
import { Avatar } from "@material-ui/core";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import ChatBubbleOutlineIcon from "@material-ui/icons/ChatBubbleOutline";
import RepeatIcon from "@material-ui/icons/Repeat";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import CreateIcon from "@mui/icons-material/Create";
import db from "./Firebase";
import { deleteDoc, doc } from "firebase/firestore";

const removePost = (post) => {
  db.collection("Posts")
    .doc(post.id)
    .delete()
    .then(() => {});
};

//TODO
function updatePost() {
  const docRef = doc(db, "Posts", "TODO");
}

const Post = forwardRef(
  ({ displayName, username, verified, text, image, avatar, id }, ref) => {
    var notUrs = false;
    return (
      <div className='post' ref={ref}>
        <div className='post__avatar'>
          <Avatar src={avatar} />
        </div>
        <div className='post__body'>
          <div className='post__header'>
            <div className='post__headerText'>
              <h3>
                {displayName}{" "}
                <span className='post__headerSpecial'>
                  {verified && <VerifiedUserIcon className='post__badge' />} @
                  {username}
                </span>
              </h3>
            </div>
            <div className='post__headerDescription'>
              <p>{text}</p>
            </div>
          </div>
          <img src={image} alt='' />
          <div className='post__footer'>
            <button onClick={() => removePost(id)} hidden={notUrs}>
              <DeleteForeverIcon fontSize='medium' />
            </button>
            <button onClick={updatePost} hidden={notUrs}>
              <CreateIcon fontSize='small' />
            </button>
          </div>
        </div>
      </div>
    );
  }
);

export default Post;
