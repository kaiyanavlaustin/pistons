import React, { useState } from "react";
import { signInWithEmailAndPassword } from "firebase/auth";
//import "./Login.css";
import { useNavigate } from "react-router-dom";
import { auth } from "../components/Firebase-config";

export default function Login() {
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  const navigate = useNavigate();

  const login = async () => {
    try {
      const user = await signInWithEmailAndPassword(
        auth,
        loginEmail,
        loginPassword
      );
      navigate("/home");
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <div>
      <h3> Login </h3>
      <input
        placeholder='Email...'
        type='email'
        onChange={(event) => {
          setLoginEmail(event.target.value);
        }}
      />
      <input
        placeholder='Password...'
        type='password'
        onChange={(event) => {
          setLoginPassword(event.target.value);
        }}
      />

      <button onClick={login}>Login</button>
      <br />
      <p>
        new? <a href='/register'>Create a new account here</a>
      </p>
    </div>
  );
}
