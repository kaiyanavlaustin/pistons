import React, { useState, useEffect } from "react";
import PostBox from "../components/postBox";
import Post from "./Post";
import "../css/Feed.css";
import db from "./Firebase";
import FlipMove from "react-flip-move";
import { auth } from "./Firebase-config";
import "firebase/firestore";
import { collection, query, where, onSnapshot, doc } from "firebase/firestore";

function Feed() {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    if (auth.currentUser) {
      let uid = auth.currentUser.uid;

      db.collection("user").doc(uid);
    } else {
    }
  }, [auth]);

  
  const [id, setID] = useState("");
  
  useEffect(() => {
    db.collection("Posts").onSnapshot((snapshot) => {
      setPosts(snapshot.docs.map((doc) => doc.data()));
      setID({ ...doc.id, id: doc.id });
    });
  }, []);
  const uid = auth.currentUser.uid;
  const userRef = collection(db, "users");
  const userQuery = query(userRef, where("UUID", "==", uid));
  const [user, setUser] = useState([]);

  onSnapshot(userQuery, (snapshot) => {
    let user = [];
    snapshot.docs.forEach((doc) => {
      user.push({ ...doc.data(), id: doc.id });
      setUser({ ...doc.data(), id: doc.id });
    });
  });

  return (
    <div className='feed'>
      <div className='feed__header'>
        <h2>Welcome Home, {user.displayName}</h2>
      </div>

      <PostBox />

      <FlipMove>
        {posts.map((post) => (
          <Post
            key={post}
            displayName={post.displayName}
            username={post.username}
            verified={post.verified}
            text={post.text}
            avatar={post.avatar}
            image={post.image}
            id={post}
          />
        ))}
      </FlipMove>
    </div>
  );
}

export default Feed;
