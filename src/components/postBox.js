import React, { useState } from "react";
import "../css/postBox.css";
import { Avatar, Button } from "@material-ui/core";
import db from "./Firebase";
import {
  collection,
  query,
  where,
  onSnapshot,

} from "firebase/firestore";
import { auth } from "./Firebase-config";

function PostBox() {
  const [postMessage, setpostMessage] = useState("");
  const [postImage, setpostImage] = useState("");

  const uid = auth.currentUser.uid;
  const userRef = collection(db, "users");
  const userQuery = query(userRef, where("UUID", "==", uid));
  const [user, setUser] = useState([]);

  onSnapshot(userQuery, (snapshot) => {
    let user = [];
    snapshot.docs.forEach((doc) => {
      user.push({ ...doc.data(), id: doc.id });
      setUser({ ...doc.data(), id: doc.id });
    });
  });

  const sendPost = (e) => {
    e.preventDefault();

    db.collection("Posts").add({
      id: this.doc().id,
      displayName: user.displayName,
      username: user.userName,
      text: postMessage,
      verified: true,
      image: postImage,
      avatar: user.avatar,
      UUID: user.UUID,
    });

    setpostMessage("");
    setpostImage("");
  };

  return (
    <div className='postbox'>
      <form>
        <div className='postbox__input'>
          <Avatar src={user.avatar} />
          <input
            onChange={(e) => setpostMessage(e.target.value)}
            value={postMessage}
            placeholder="What's happening?"
            type='text'
          />
        </div>
        <input
          value={postImage}
          onChange={(e) => setpostImage(e.target.value)}
          className='postbox__imageInput'
          placeholder='Optional: Enter image URL'
          type='text'
        />

        <Button
          onClick={sendPost}
          type='submit'
          className='postbox__tweetButton'
        >
          Post!
        </Button>
      </form>
    </div>
  );
}

export default PostBox;
