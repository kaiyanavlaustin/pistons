import React from "react";
import "../css/Widgets.css";

import {
  TwitterTimelineEmbed,
  TwitterShareButton,
  TwitterTweetEmbed,
} from "react-twitter-embed";
import SearchIcon from "@material-ui/icons/Search";

function Widgets() {
  return (
    <div className='widgets'>
      <div className='widgets__input'>
        <SearchIcon className='widgets__searchIcon' />
        <input placeholder='Search Twitter' type='text' />
      </div>

      <div className='widgets__widgetContainer'>
        <h2>Whats happening?</h2>
        <TwitterShareButton />
        <TwitterTweetEmbed tweetId={"1551566417543266305"} />
        <TwitterTimelineEmbed
          sourceType='profile'
          screenName='killarney_ct'
          options={{ height: 400 }}
        />
      </div>
    </div>
  );
}

export default Widgets;
