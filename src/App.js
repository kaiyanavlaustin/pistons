
import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Profile from "./pages/Profile"
import Login from "./pages/Login";
import Register from "./pages/Register";

// import SignUp from "./Signup";
import { AuthProvider } from "../src/components/AuthProvider";

export default function App() {
  return (
      <div className='app'>
        <BrowserRouter>
          <Routes>

            <Route exact path='/' element={<Login />} />
            <Route exact path='/register' element={<Register />} />
            <Route exact path='/home' element={<Home />} />
            <Route exact path='/profile' element={<Profile />} />
            {/* <Route exact path='/signup' element={<SignUp />} /> */}

            <Route exact path='/' element={<Home />} />
            <Route exact path='/login' element={<Login />} />
            <Route exact path='/signup' element={<Register />} />

          </Routes>
        </BrowserRouter>
      </div>
  );
}
